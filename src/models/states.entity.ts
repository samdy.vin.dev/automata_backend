import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { FA } from './fa.entity'

@Entity('state')
export class State {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column()
  name: string

  @Column({ name: 'is_start' })
  is_start: boolean

  @Column({ name: 'is_final' })
  is_final: boolean

  @Column({ name: 'is_dead', default: false })
  is_dead: boolean

  @ManyToOne((type) => FA, (dfa) => dfa.states, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({
    name: 'fa_id',
  })
  fa: FA
}
