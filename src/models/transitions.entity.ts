import {
  Column,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { State } from './states.entity'
import { TransitionTable } from './transition_tables.entity'

@Entity('transitions')
export class Transition {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @ManyToOne((type) => State, { onUpdate: 'CASCADE', onDelete: 'CASCADE' })
  @JoinColumn({ name: 'from_state_id', referencedColumnName: 'id' })
  from: State

  @ManyToMany((type) => State, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinTable({
    name: 'transition_to_states',
    joinColumn: { name: 'transition_id', referencedColumnName: 'id' },
    inverseJoinColumn: { name: 'to_state_id', referencedColumnName: 'id' },
  })
  to: State[]

  @Column()
  input: string

  @ManyToOne(
    (type) => TransitionTable,
    (transitionTable) => transitionTable.transitions,
    {
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    },
  )
  @JoinColumn({ name: 'transition_table_id' })
  transition_table: TransitionTable
}
