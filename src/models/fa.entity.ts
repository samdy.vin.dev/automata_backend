import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'
import { State } from './states.entity'
import { TransitionTable } from './transition_tables.entity'

@Entity('fa')
export class FA {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({ nullable: true })
  name: string

  @Column()
  symbols: string

  @OneToMany((type) => State, (state) => state.fa, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  states: State[]

  @OneToOne(
    (type) => TransitionTable,
    (transition_table) => transition_table.fa,
    { onDelete: 'CASCADE', onUpdate: 'CASCADE' },
  )
  transition_table: TransitionTable

  @Column({ nullable: true })
  user_id: string

  @CreateDateColumn()
  created_at: Date

  @UpdateDateColumn()
  updated_at: Date
}
