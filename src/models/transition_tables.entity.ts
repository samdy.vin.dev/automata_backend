import {
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { FA } from './fa.entity'
import { Transition } from './transitions.entity'

@Entity('transition_tables')
export class TransitionTable {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @OneToMany(
    (type) => Transition,
    (transition) => transition.transition_table,
    {
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    },
  )
  transitions: Transition[]

  @OneToOne((type) => FA, (fa) => fa.transition_table, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn({ name: 'fa_id' })
  fa: FA
}
