import {
  Body,
  Controller,
  Get,
  Headers,
  Post,
  Param,
  Delete,
} from '@nestjs/common'
import {
  AcceptStringDto,
  CheckFaDto,
  ConvertNfaToDfaDto,
  DesignFaDto,
  MinimizeDfaDto,
} from './dto'
import { HomeService } from './home.service'

@Controller('')
export class HomeController {
  constructor(private readonly homeService: HomeService) {}

  @Get('fa')
  async findFa(@Headers('user_id') user_id: string) {
    return this.homeService.findFa(user_id)
  }

  @Delete('fa/:fa_id')
  async deleteFa(
    @Param('fa_id') fa_id: string,
    @Headers('user_id') user_id: string,
  ) {
    return this.homeService.deleteFa(fa_id, user_id)
  }

  @Post('design-fa')
  async designFa(
    @Body() data: DesignFaDto,
    @Headers('user_id') user_id: string,
  ) {
    return this.homeService.designFa(data, user_id)
  }

  @Post('check-fa')
  async checkFa(@Body() data: CheckFaDto) {
    return this.homeService.checkFa(data)
  }

  @Post('accept-string')
  async acceptString(@Body() data: AcceptStringDto) {
    return this.homeService.acceptString(data)
  }

  @Post('convert-nfa-to-dfa')
  async convertNfaToDfa(@Body() data: ConvertNfaToDfaDto) {
    return this.homeService.convertNfaToDfa(data)
  }

  @Post('minimize-dfa')
  async minimizeDfa(@Body() data: MinimizeDfaDto) {
    return this.homeService.minimizeDfa(data)
  }
}
