import { FA } from '@/models'
import { IsNotEmpty } from 'class-validator'

export class ConvertNfaToDfaDto {
  @IsNotEmpty()
  fa: FA
}
