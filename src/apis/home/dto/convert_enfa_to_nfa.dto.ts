import { FA } from '@/models'
import { IsNotEmpty } from 'class-validator'

export class ConvertENFAtoNFA {
  @IsNotEmpty()
  fa: FA
}
