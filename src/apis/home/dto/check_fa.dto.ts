import { FA } from '@/models'
import { IsNotEmpty } from 'class-validator'

export class CheckFaDto {
  @IsNotEmpty()
  fa: FA
}
