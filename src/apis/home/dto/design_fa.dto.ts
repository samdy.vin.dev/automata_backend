import { FA, State } from '@/models'
import { TransitionTable } from '@/models/transition_tables.entity'
import { OmitType } from '@nestjs/mapped-types'
import { IsArray, IsNotEmpty, IsString } from 'class-validator'

export class DesignFaDto extends OmitType(FA, ['id']) {
  name!: string

  @IsNotEmpty({ message: 'States is required' })
  @IsArray({ message: 'States must be an array' })
  states: State[]

  @IsNotEmpty({ message: 'Symbols is required' })
  @IsString({ message: 'Symbols must be a string' })
  symbols: string

  @IsNotEmpty({ message: 'Transition table is required' })
  transition_table: TransitionTable
}
