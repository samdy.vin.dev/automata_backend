import { TransitionTable, State } from '@/models'
import { IsNotEmpty } from 'class-validator'

export class NextStatesOfState {
  @IsNotEmpty()
  transition_table: TransitionTable

  @IsNotEmpty()
  state: State
}

export class nextStatesOfStateBySymbol {
  @IsNotEmpty()
  transition_table: TransitionTable

  @IsNotEmpty()
  state: State

  @IsNotEmpty()
  symbol: string
}
