import { FA, State } from '@/models'
import { IsNotEmpty } from 'class-validator'

export class MinimizeDfaDto {
  @IsNotEmpty()
  fa: FA
}

export class StatePair {
  id!: string
  states: [State, State]
  is_marked: boolean
}
