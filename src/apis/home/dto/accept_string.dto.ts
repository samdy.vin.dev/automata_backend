import { FA } from '@/models'
import { IsNotEmpty, IsString } from 'class-validator'

export class AcceptStringDto {
  @IsNotEmpty()
  @IsString()
  text: string

  @IsNotEmpty()
  fa: FA
}
