import { State, TransitionTable } from '@/models'
import { IsNotEmpty } from 'class-validator'

export class EClosureDto {
  @IsNotEmpty()
  transition_table: TransitionTable

  @IsNotEmpty()
  state: State
}

export class EClosureByStateOnInputDto {
  @IsNotEmpty()
  transition_table: TransitionTable

  @IsNotEmpty()
  state: State

  @IsNotEmpty()
  input: string
}
