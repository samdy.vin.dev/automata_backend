import { FA, State, Transition, TransitionTable } from '@/models'
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import * as _ from 'lodash'
import { In, Repository } from 'typeorm'
import {
  AcceptStringDto,
  CheckFaDto,
  ConvertNfaToDfaDto,
  DesignFaDto,
  MinimizeDfaDto,
  StatePair,
} from './dto'
import { ConvertENFAtoNFA } from './dto/convert_enfa_to_nfa.dto'
import { EClosureByStateOnInputDto, EClosureDto } from './dto/e_closure.dto'
import { nextStatesOfStateBySymbol } from './dto/next_state_of_state.dto'

const relations = [
  'states',
  'transition_table',
  'transition_table.transitions',
  'transition_table.transitions.from',
  'transition_table.transitions.to',
]

@Injectable()
export class HomeService {
  constructor(
    @InjectRepository(FA)
    private faRepository: Repository<FA>,

    @InjectRepository(State)
    private stateRepository: Repository<State>,

    @InjectRepository(TransitionTable)
    private transitionTableRepository: Repository<TransitionTable>,

    @InjectRepository(Transition)
    private transitionRepository: Repository<Transition>,
  ) {}

  async findFa(user_id: string) {
    const fa = await this.faRepository.find({
      where: { user_id },
      relations: relations,
    })
    return {
      message: 'Get FA Successfully',
      payload: fa,
    }
  }

  async deleteFa(fa_id: string, user_id: string) {
    const fa = await this.faRepository.findOneOrFail({
      where: { id: fa_id },
      relations: relations,
    })
    if (fa.user_id !== user_id) {
      throw new BadRequestException('You are not owner of this FA')
    }
    await this.faRepository.delete(fa_id)
    return {
      message: 'Delete FA Successfully',
    }
  }

  async designFa(data: DesignFaDto, user_id: string) {
    // Create new FA to database
    const fa = await this.faRepository.save(
      this.faRepository.create({
        name: data.name,
        symbols: data.symbols,
        user_id: user_id,
      }),
    )

    // Create new Transition Table to database
    const savedTransitionTable = await this.transitionTableRepository.save(
      this.transitionTableRepository.create({
        fa,
      }),
    )

    // Format all requested states to database
    const formatedStates = data.states.map((state) =>
      this.stateRepository.create({
        ...state,
        fa: fa,
      }),
    )

    // Save all states to database
    const savedStates = await this.stateRepository.save(formatedStates)

    // Format all requested transitions to database
    const formatedTransitions = data.transition_table.transitions.map(
      (transition) =>
        this.transitionRepository.create({
          ...transition,
          from: savedStates.find(
            (state) => state.name === transition.from.name,
          ),
          to: transition.to.map((state) =>
            savedStates.find((savedState) => savedState.name === state.name),
          ),
          transition_table: savedTransitionTable,
        }),
    )

    // Save all transitions to database
    const savedTransitions = await this.transitionRepository.save(
      formatedTransitions,
    )

    // Save all changes to database
    savedTransitionTable.transitions = savedTransitions
    fa.states = savedStates
    fa.transition_table = savedTransitionTable
    await this.transitionTableRepository.save(savedTransitionTable)
    await this.faRepository.save(fa)
    const savedFa = await this.faRepository.findOne({
      where: { id: fa.id },
      relations: relations,
    })

    return {
      message: 'Design FA successfully',
      paylaod: {
        ...savedFa,
        start_state: savedFa.states.find((state) => state.is_start),
        final_states: savedFa.states.filter((state) => state.is_final),
      },
    }
  }

  async checkFa(data: CheckFaDto) {
    let isDFA = true

    // Check if FA is existed
    const fa = await this.faRepository
      .findOneOrFail({
        where: { id: data.fa.id },
        relations: relations,
      })
      .catch((_) => {
        throw new NotFoundException('FA not found')
      })

    // Check if symbol include ε
    if (fa.symbols.includes('ε')) {
      isDFA = false
    }

    // Check all transitions
    for (const transition of fa.transition_table.transitions) {
      // Check if transition is valid
      if (transition.to.length > 1) {
        isDFA = false
      }

      if (transition.to.length === 0) {
        isDFA = false
      }
    }

    return {
      message: isDFA ? 'This FA is DFA' : 'This FA is NFA',
      paylaod: { isDFA, fa },
    }
  }

  async acceptString(data: AcceptStringDto) {
    let accepted = true
    // Check if FA is existed
    const existedFa = await this.faRepository
      .findOneOrFail({
        where: { id: data.fa.id },
        relations: relations,
      })
      .catch((_) => {
        throw new NotFoundException('FA not found')
      })

    let fa = existedFa

    let isDFA = (await this.checkFa({ fa: existedFa })).paylaod.isDFA

    // If FA is not a DFA, convert it to DFA
    if (!isDFA) {
      // If FA is a ENFA, convert it to NFA
      if (fa.symbols.includes('ε')) {
        fa = (await this.convertENFAtoNFA({ fa: existedFa })).payload.nfa
      }
      // convert NFA to DFA
      fa = (await this.convertNfaToDfa({ fa: fa })).payload.dfa
    }

    // Start from start state
    let currentState = fa.states.find((state) => state.is_start)

    // Check all symbols
    for (const symbol of data.text) {
      // Check if symbol is valid
      if (!fa.symbols.includes(symbol)) {
        throw new NotFoundException(`Symbol not found (${symbol})`)
      }

      // Update current state
      currentState = fa.transition_table.transitions.find(
        (transition) =>
          transition.input === symbol && transition.from.id === currentState.id,
      )?.to[0]

      // Check if current state is valid
      if (!currentState) {
        throw new NotFoundException(`State not found (${currentState.name})`)
      }
    }

    // Check if current state is final
    if (!currentState.is_final) {
      accepted = false
    }

    return {
      message: `String is ${accepted ? 'Accepted' : 'Rejected'}`,
      paylaod: {
        isAccepted: accepted,
        currentState,
      },
    }
  }

  async convertNfaToDfa(data: ConvertNfaToDfaDto) {
    // Check if FA is existed
    let fa = await this.faRepository
      .findOneOrFail({
        where: { id: data.fa.id },
        relations: relations,
      })
      .catch((_) => {
        throw new NotFoundException('FA not found')
      })

    // Check if FA is NFA
    const isDFA = (await this.checkFa({ fa })).paylaod.isDFA

    if (isDFA) {
      throw new BadRequestException('This FA is not NFA')
    }

    // Check if FA is ENFA and convert to NFA
    fa = (await this.convertENFAtoNFA({ fa })).payload.nfa

    // Create new DFA
    const newDFA = await this.faRepository.save(
      this.faRepository.create({
        symbols: fa.symbols,
        states: [],
      }),
    )

    // Create new transition table
    const newTransitionTable = await this.transitionTableRepository.save(
      this.transitionTableRepository.create({
        transitions: [],
        fa: newDFA,
      }),
    )

    // Initialize new transitions
    const implementedStates: State[] = []
    const statesToRun: State[] = []
    const newStartState = await this.stateRepository.save(
      this.stateRepository.create({
        name: 'q0',
        is_start: true,
        is_final: false,
        fa: newDFA,
      }),
    )

    const createdStates: State[] = [newStartState]

    statesToRun.push(newStartState)
    implementedStates.push(newStartState)

    while (statesToRun.length > 0) {
      // Get current state
      const currentState = statesToRun[0]

      if (currentState.is_dead) {
        for (const symbol of fa.symbols) {
          const _newTransition = this.transitionRepository.create({
            input: symbol,
            from: currentState,
            to: [currentState],
            transition_table: newTransitionTable,
          })
          const savedTransition = await this.transitionRepository.save(
            _newTransition,
          )
          newTransitionTable.transitions.push(savedTransition)
        }
      } else {
        const includedStateNames = currentState.name.split('_')
        const includedStates = fa.states.filter((state) =>
          includedStateNames.includes(state.name),
        )

        for (const symbol of fa.symbols.split(',')) {
          const _newTransition = this.transitionRepository.create({
            input: symbol,
            from: currentState,
            to: [],
            transition_table: newTransitionTable,
          })

          let newStateNamesForNewTransition = []

          for (const selectedState of includedStates) {
            // Find old transition
            const _oldTransition = fa.transition_table.transitions.find(
              (transition) =>
                transition.input === symbol &&
                transition.from.id === selectedState.id,
            )
            if (_oldTransition) {
              const names = _oldTransition.to.map((state) => state.name).sort()
              newStateNamesForNewTransition = [
                ...newStateNamesForNewTransition,
                ...names,
              ]
            }
          }

          const convertedName = newStateNamesForNewTransition.sort().join('_')

          if (convertedName === '') {
            const newDeadState = this.stateRepository.create({
              is_dead: true,
              is_final: false,
              is_start: false,
              name: `${currentState.name}*${symbol}`,
              fa: newDFA,
            })
            const savedDeadState = await this.stateRepository.save(newDeadState)

            _newTransition.to = [savedDeadState]
          } else {
            const existedState = createdStates.find(
              (state) => state.name === convertedName,
            )

            if (!existedState) {
              const newState = this.stateRepository.create({
                is_dead: false,
                is_start: false,
                is_final: convertedName.includes(
                  fa.states.find((state) => state.is_final).name,
                ),
                name: convertedName,
                fa: newDFA,
              })
              const savedNewState = await this.stateRepository.save(newState)

              statesToRun.push(savedNewState)
              createdStates.push(savedNewState)
              _newTransition.to = [savedNewState]
              const savedNewTransition = await this.transitionRepository.save(
                _newTransition,
              )
              newTransitionTable.transitions.push(savedNewTransition)
            } else {
              _newTransition.to = [existedState]

              const savedNewTransition = await this.transitionRepository.save(
                _newTransition,
              )
              newTransitionTable.transitions.push(savedNewTransition)
            }
          }
        }
      }

      // Remove the current state from states to run
      statesToRun.shift()

      // Add current state to implemented states
      implementedStates.push(currentState)
    }

    // Update new transition table
    await this.transitionTableRepository.save(newTransitionTable)

    const savedNewDFA = await this.faRepository.findOne({
      where: {
        id: newDFA.id,
      },
      relations: relations,
    })

    return {
      message: 'Convert NFA to DFA successfully',
      payload: {
        dfa: savedNewDFA,
        nfa: fa,
      },
    }
  }

  async nextStatesOfStateBySymbol(
    data: nextStatesOfStateBySymbol,
  ): Promise<State[]> {
    let states: State[] = []
    data.transition_table.transitions.forEach((transition) => {
      if (
        transition.from.id === data.state.id &&
        transition.input === data.symbol
      ) {
        states = [...states, ...transition.to]
      }
    })
    return states
  }

  async eClosure(data: EClosureDto): Promise<State[]> {
    const { state, transition_table } = data
    let states: State[] = [state]

    transition_table.transitions.forEach((transition) => {
      if (transition.from.id === state.id && transition.input === 'ε') {
        states = [...states, ...transition.to]
      }
    })

    return states
  }

  async eClosureByStateOnInput(
    data: EClosureByStateOnInputDto,
  ): Promise<State[]> {
    const { state, transition_table, input } = data
    let firstStageStates: State[] = []
    let secondStageStates: State[] = []
    let finalStageStates: State[] = []

    // Calculate First Stage State
    firstStageStates = await this.eClosure({ state, transition_table })

    // Calculate Second Stage State
    firstStageStates.forEach(async (firstStageState) => {
      const _nextStates = await this.nextStatesOfStateBySymbol({
        state: firstStageState,
        transition_table,
        symbol: input,
      })
      secondStageStates = [...secondStageStates, ..._nextStates]
    })

    // Calculate Final Stage State
    secondStageStates.forEach(async (secondStageState) => {
      const _nextStates = await this.eClosure({
        state: secondStageState,
        transition_table,
      })
      finalStageStates = [...finalStageStates, ..._nextStates]
    })

    return finalStageStates
  }

  async convertENFAtoNFA(data: ConvertENFAtoNFA) {
    const existed = await this.faRepository.findOne({
      where: { id: data.fa.id },
      relations: relations,
    })
    if (!existed) {
      throw new NotFoundException('FA not found')
    }

    // Check if FA is not a ENFA
    if (!existed.symbols.includes('ε')) {
      return {
        message: 'Converted successfully',
        payload: {
          nfa: existed,
          enfa: existed,
        },
      }
    }

    const newNFA = await this.faRepository.save(
      this.faRepository.create({
        symbols: existed.symbols
          .split(',')
          .filter((symbol) => symbol !== 'ε')
          .join(','),
        states: existed.states,
      }),
    )

    const newTransitionTable = await this.transitionTableRepository.save(
      this.transitionTableRepository.create({
        transitions: [],
        fa: newNFA,
      }),
    )

    for (const state of newNFA.states) {
      for (const symbol of newNFA.symbols.split(',')) {
        const _newTransition = await this.transitionRepository.save(
          this.transitionRepository.create({
            from: state,
            input: symbol,
            transition_table: newTransitionTable,
            to: await this.nextStatesOfStateBySymbol({
              state: state,
              symbol: symbol,
              transition_table: existed.transition_table,
            }),
          }),
        )
      }
    }

    const savedNewNFA = await this.faRepository.findOne({
      where: { id: newNFA.id },
      relations: [
        'states',
        'transition_table',
        'transition_table.transitions',
        'transition_table.transitions.from',
        'transition_table.transitions.to',
      ],
    })

    return {
      message: 'Converted Successfully',
      payload: {
        nfa: savedNewNFA,
        enfa: existed,
      },
    }
  }

  filterMarkedPair(pair: StatePair): StatePair {
    let ok = 0

    for (const state of pair.states) {
      if (state.is_final) {
        ok++
      }
    }

    const result = new StatePair()
    result.states = pair.states

    if (ok === 1) {
      result.is_marked = true
    } else {
      result.is_marked = false
    }

    return result
  }

  async minimizeDfa(data: MinimizeDfaDto) {
    // There are 4 steps
    // 1. Find all pairs of states
    // 2. Mark all pairs where one state is final and the other is not final
    // 3. If there are any Unmarked pairs (P,Q),such that [S(P,x),S(Q,x)] is unmarked then mark [P,Q] where x is an input symbol
    // 4. Combind all the unmarked pairs into one state
    const _oldID = data.fa.id

    const fa = await this.faRepository.findOne({
      where: { id: _oldID },
      relations: [
        'states',
        'transition_table',
        'transition_table.transitions',
        'transition_table.transitions.from',
        'transition_table.transitions.to',
      ],
    })
    if (!fa) {
      throw new NotFoundException('FA not found')
    }

    const isDfa = (await this.checkFa({ fa: fa })).paylaod.isDFA

    if (!isDfa) {
      throw new BadRequestException('FA is not a DFA')
    }

    const isMinimal = fa.states.length < 4

    if (isMinimal) {
      return {
        message: 'FA is already minimal',
        payload: {
          minimizedDfa: fa,
        },
      }
    }

    const newDFA = await this.faRepository.save(
      this.faRepository.create({
        symbols: fa.symbols,
        states: [],
      }),
    )

    const newTransitionTable = await this.transitionTableRepository.save(
      this.transitionTableRepository.create({
        transitions: [],
        fa: newDFA,
      }),
    )

    // 1. Find all pairs of states
    console.log('\n\n1. Find all pairs of states\n\n')
    let pairs: StatePair[] = []
    const stateNames = fa.states.map((state) => state.name).sort()
    const firstItem = stateNames[0]
    const lastItem = stateNames[stateNames.length - 1]
    const horizontalStateNames = stateNames.filter((x) => x !== firstItem)

    const verticalStateNames = stateNames.filter((x) => x !== lastItem)

    const horizontalStates = _.sortBy(
      fa.states.filter((state) => horizontalStateNames.includes(state.name)),
      'name',
    )

    const verticalStates = _.sortBy(
      fa.states.filter((state) => verticalStateNames.includes(state.name)),
      'name',
    )

    console.log(
      'horizontalStates:',
      horizontalStates.map((state) => state.name),
    )
    console.log(
      'verticalStates:',
      verticalStates.map((state) => state.name),
    )

    for (const h_state of horizontalStates) {
      for (const v_state of verticalStates) {
        pairs.push({
          states: [h_state, v_state],
          is_marked: false,
        } as StatePair)
      }
    }

    let filteredDuplicatePair: StatePair[] = []

    for (const pair of pairs) {
      const isDuplicate = pair.states[0].id === pair.states[1].id
      const names = _.sortBy(pair.states.map((state) => state.name)).join(',')
      const existedNames = _.sortBy(
        filteredDuplicatePair.map((pair) =>
          _.sortBy(pair.states.map((state) => state.name)).join(','),
        ),
      )
      const isDuplicateName = existedNames.includes(names)

      if (!isDuplicate && !isDuplicateName) {
        filteredDuplicatePair.push(pair)
      }
    }

    const filteredNames = _.sortBy(
      filteredDuplicatePair.map((pair) =>
        _.sortBy(pair.states.map((state) => state.name)).join(','),
      ),
    )

    console.log('filteredPair:', filteredNames)

    // 2. Mark all pairs where one state is final and the other is not final
    console.log(
      '\n\n2. Mark all pairs where one state is final and the other is not final\n\n',
    )
    let markedPairs: StatePair[] = []
    let unMarkedPairs: StatePair[] = []

    for (const pair of filteredDuplicatePair) {
      const _pair = this.filterMarkedPair(pair)
      if (_pair.is_marked) {
        markedPairs.push(_pair)
      } else {
        unMarkedPairs.push(_pair)
      }
    }

    // Assign unique id to all pairs
    markedPairs.forEach((pair, index) => {
      pair.id = _.uniqueId()
    })
    unMarkedPairs.forEach((pair, index) => {
      pair.id = _.uniqueId()
    })
    console.log(
      'markedPairs:',
      markedPairs.map(
        (pair) => `${pair.states[0].name},${pair.states[1].name}`,
      ),
    )
    console.log(
      'unMarkedPairs:',
      unMarkedPairs.map(
        (pair) => `${pair.states[0].name},${pair.states[1].name}`,
      ),
    )

    // 3. If there are any Unmarked pairs (P,Q),such that [S(P,x),S(Q,x)] is unmarked then mark [P,Q] where x is an input symbol
    console.log(
      '\n\n3. If there are any Unmarked pairs (P,Q),such that [S(P,x),S(Q,x)] is unmarked then mark [P,Q] where x is an input symbol\n\n',
    )
    let oldLength = 0

    console.log('Start the loop of (unMarkedPairs):')

    while (true) {
      oldLength = unMarkedPairs.length
      console.log('-----------------------------------------------')
      console.log('while loop:')
      for (const pair of unMarkedPairs) {
        console.log('\n\n========================')
        console.log(`Main Pair: ${pair.states[0].name},${pair.states[1].name}`)
        for (const symbol of fa.symbols.split(',')) {
          console.log('------------------------')
          console.log('symbol:', symbol)
          const _pair = new StatePair()
          const firstElement = fa.transition_table.transitions.find(
            (transition) =>
              transition.from.id === pair.states[0].id &&
              transition.input === symbol,
          ).to[0]
          const secondElement = fa.transition_table.transitions.find(
            (transition) =>
              transition.from.id === pair.states[1].id &&
              transition.input === symbol,
          ).to[0]
          _pair.states = [firstElement, secondElement]
          _pair.states = _pair.states.sort()

          const isMarked =
            (firstElement.is_final && !secondElement.is_final) ||
            (!firstElement.is_final && secondElement.is_final)
          _pair.is_marked = isMarked

          if (isMarked) {
            markedPairs.push(pair)
            unMarkedPairs = unMarkedPairs.filter((x) => x.id !== pair.id)
          }
          console.log(`==> pair: [${firstElement.name},${secondElement.name}]`)
          console.log(` ==> Status: ${isMarked ? '✅' : '❌'}`)
        }
      }
      console.log('oldLength => ', oldLength)
      console.log('newLength => ', unMarkedPairs.length)

      if (oldLength === unMarkedPairs.length) {
        break
      }
    }

    console.log(
      'unMarkedPairs:',
      _.sortBy(
        unMarkedPairs.map((pair) =>
          _.sortBy(pair.states.map((state) => state.name)).join(','),
        ),
      ),
    )

    // 4. Combind all the unmarked pairs into one state
    console.log('\n\n4. Combind all the unmarked pairs into one state\n\n')
    const newStates: State[] = []
    for (const pair of unMarkedPairs) {
      const stateName = _.sortBy(pair.states.map((state) => state.name)).join(
        ',',
      )
      const newState = this.stateRepository.create({
        name: stateName,
        is_final: stateName.includes(fa.states.find((x) => x.is_final).name),
        is_dead: false,
        is_start: stateName.includes(fa.states.find((x) => x.is_start).name),
        fa: newDFA,
      })
      const savedNewState = await this.stateRepository.save(newState)

      newStates.push(savedNewState)

      for (const symbol of newDFA.symbols.split(',')) {
        const oldTransition = fa.transition_table.transitions.find(
          (transition) =>
            transition.from.id === pair.states[0].id &&
            transition.input === symbol,
        )

        const transition = await this.transitionRepository.save(
          this.transitionRepository.create({
            from: savedNewState,
            to: [oldTransition.to[0]],
            input: symbol,
            transition_table: newTransitionTable,
          }),
        )
        await this.transitionRepository.save(transition)
      }
    }

    const filteredCombindedStates: State[] = []

    for (const state of fa.states) {
      const isUnMarkedState = unMarkedPairs.some((x) =>
        x.states.includes(state),
      )
      if (!isUnMarkedState) {
        const newState = await this.stateRepository.save(
          this.stateRepository.create({
            name: state.name,
            is_final: state.is_final,
            is_dead: state.is_dead,
            is_start: state.is_start,
            fa: newDFA,
          }),
        )
        filteredCombindedStates.push(newState)

        for (const symbol of newDFA.symbols.split(',')) {
          let oldState = fa.transition_table.transitions.find(
            (transition) =>
              transition.from.id === state.id && transition.input === symbol,
          ).to[0]

          const transition = await this.transitionRepository.save(
            this.transitionRepository.create({
              from: newState,
              to: [oldState],
              input: symbol,
              transition_table: newTransitionTable,
            }),
          )
          await this.transitionRepository.save(transition)
        }
      }
    }

    const resultStates = [...filteredCombindedStates, ...newStates]

    const allTransitions = await this.transitionRepository.find({
      relations: ['to'],
      where: {
        transition_table: newTransitionTable,
      },
    })

    const usedStates: State[] = _.uniqWith(
      allTransitions.map((x) => x.to[0]),
      _.isEqual,
    )

    const unusedStates = _.differenceBy(
      filteredCombindedStates,
      usedStates,
      (x) => x.name,
    )

    // Remove unused states
    const savedUnusState = await this.stateRepository.findBy({
      id: In(unusedStates.map((x) => x.id)),
    })
    await this.stateRepository.remove(savedUnusState)

    // Update transition table
    for (const transition of allTransitions) {
      const toName = transition.to[0].name
      const existedInNewStates = newStates.filter((x) =>
        x.name.includes(toName),
      )

      if (existedInNewStates.length > 0) {
        await this.transitionRepository.save({
          ...transition,
          to: [existedInNewStates[0]],
        })
      }
    }

    let minimizedDfa = await this.faRepository.findOne({
      where: { id: newDFA.id },
      relations: relations,
    })

    const formatedTable = _.sortBy(
      minimizedDfa.transition_table.transitions.map((transition) => {
        const isFinal = transition.from.is_final
        const isStart = transition.from.is_start

        if (isFinal) {
          return {
            from: `*${transition.from.name}`,
            to: transition.to[0].name,
            input: transition.input,
          }
        } else if (isStart) {
          return {
            from: `->${transition.from.name}`,
            to: transition.to[0].name,
            input: transition.input,
          }
        } else {
          return {
            from: transition.from.name,
            to: transition.to[0].name,
            input: transition.input,
          }
        }
      }),
      'from',
    )

    console.table(formatedTable)

    return {
      message: 'DFA is minimized successfully',
      payload: {
        minimizedDfa,
      },
    }
  }
}
