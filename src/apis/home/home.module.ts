import { Module } from '@nestjs/common'
import { HomeService } from './home.service'
import { HomeController } from './home.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { FA, State, TransitionTable, Transition } from '@/models'

@Module({
  imports: [TypeOrmModule.forFeature([FA, State, TransitionTable, Transition])],
  controllers: [HomeController],
  providers: [HomeService],
})
export class HomeModule {}
