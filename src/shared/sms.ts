const accountSid = process.env.TWILIO_ACCOUNT_SID
const authToken = process.env.TWILIO_AUTH_TOKEN

// @ts-ignore
const twilio = require('twilio')(accountSid, authToken)

export const sendSms = async ({
  phoneNumber,
  message,
}: {
  phoneNumber: string
  message: string
}) => {
  await twilio.messages
    .create({
      body: message,
      from: process.env.TWILIO_NUMBER,
      to: phoneNumber,
    })
    .catch((error) => {
      throw new Error(error)
    })
}
