import { cloudinary, cloudinaryConfig } from '@/config/cloudinary.config'
import { randomUUID } from 'crypto'

class FileData {
  thumbnail!: string | null
  url!: string
  name!: string
  size!: number
  type!: string
  source_id!: string
}

type FileType = 'image' | 'video' | 'other'

export async function cldUploadSingleFile(
  file: Express.Multer.File,
  fileType: FileType,
): Promise<FileData> {
  return new Promise(async (resolve, reject) => {
    const destination = `${fileType}s`
    try {
      const fileName =
        fileType === 'other'
          ? `${randomUUID()}${file.originalname.slice(
              file.originalname.lastIndexOf('.'),
            )}`
          : `${randomUUID()}`
      const { secure_url, public_id } = await cloudinary.uploader.upload(
        file.path,
        {
          public_id: fileName,
          resource_type: fileType !== 'other' ? fileType : 'raw',
          folder: destination,
        },
      )

      const fileData = new FileData()
      fileData.name = file.originalname
      fileData.size = file.size
      fileData.type = file.mimetype
      fileData.source_id = public_id

      switch (fileType) {
        case 'video':
          fileData.thumbnail = `https://res.cloudinary.com/${cloudinaryConfig.cloud_name}/video/upload/c_lpad,h_350,w_350/${public_id}.jpg`
          fileData.url = `https://res.cloudinary.com/${cloudinaryConfig.cloud_name}/video/upload/${public_id}.mp4`

          break
        case 'image':
          fileData.thumbnail = `https://res.cloudinary.com/${cloudinaryConfig.cloud_name}/image/upload/c_lpad,h_350,w_350/${public_id}.jpg`
          fileData.url = `https://res.cloudinary.com/${cloudinaryConfig.cloud_name}/image/upload/${public_id}.jpg`

          break
        default:
          fileData.thumbnail = null
          fileData.url = secure_url

          break
      }
      resolve(fileData)
    } catch (error) {
      reject(error)
    }
  })
}

export const cldUploadMultipleFiles = async (
  files: Express.Multer.File[],
  fileType: FileType,
): Promise<FileData[]> => {
  return new Promise((resolve, reject) => {
    try {
      const result = []
      files.forEach(async (file) => {
        const uploadedFile = await cldUploadSingleFile(file, fileType)
        result.push(uploadedFile)
        if (result.length === files.length) {
          resolve(result)
        }
      })
    } catch (error) {
      reject(error)
    }
  })
}
