type Res = {
  message?: string
  payload?: Record<string, any>
  options?: Record<string, any>
}

export default function Res(res: Res) {
  return res
}
