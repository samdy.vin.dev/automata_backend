import { FindManyOptions } from 'typeorm'

type GetPaginationType = FindManyOptions & {
  modelRepo: any
  query: Record<string, any>
}

export const getPagination = async ({
  modelRepo,
  query,
  ...other
}: GetPaginationType) => {
  const take = Number(query.limit) || 10
  const skip = Number(query.page) * take

  const products = await modelRepo.findAndCount({
    take,
    skip,
    ...other,
  })

  return {
    payload: products[0],
    options: {
      total: products[1],
      current_page: query.page || 1,
      next_page: Number(query.page) + 1 || 1,
      previous_page: Number(query.page) - 1 || 1,
      last_page: Math.ceil(products[1] / take),
    },
  }
}
