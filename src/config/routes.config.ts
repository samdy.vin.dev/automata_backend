import { RequestMethod } from '@nestjs/common'
import { RouteInfo } from '@nestjs/common/interfaces'

export const publicRoutesConfig: string | RouteInfo[] = [
  { path: 'api/v1/user/connect', method: RequestMethod.POST },
  { path: 'api/v1/user/verify', method: RequestMethod.POST },
]
