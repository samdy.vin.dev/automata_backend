import * as dotenv from 'dotenv'
import { ConnectionOptions } from 'typeorm'
dotenv.config()
export const env: any = process.env

export const ormconfig: ConnectionOptions = {
  type: 'mysql',
  host: env.DB_HOST,
  port: Number(env.DB_PORT),
  username: env.DB_USER,
  password: env.DB_PASS,
  database: env.DB_NAME,
  entities: ['dist/**/*.entity.js'],
  migrations: ['dist/migrations/*.js'],
  synchronize: true,
  logging: true,
  migrationsRun: false,
  logger: 'file',
}
