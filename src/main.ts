import { ValidationPipe } from '@nestjs/common'
import { NestFactory } from '@nestjs/core'
import { MainModule } from './main.module'

// Security
import helmet from 'helmet'

async function bootstrap() {
  const app = await NestFactory.create(MainModule)
  app.useGlobalPipes(new ValidationPipe())
  app.setGlobalPrefix('api/v1')
  app.enableCors()
  app.use(helmet())
  await app.listen(process.env.PORT)
}
bootstrap()
