import { Module } from '@nestjs/common' // Require to be on top
import * as AppModule from './apis'
// Config
import { AppConfigModule } from './config'

@Module({
  imports: [AppConfigModule, ...Object.values(AppModule)],
  providers: [],
})
export class MainModule {}
