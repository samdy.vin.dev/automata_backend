FROM node:14-alpine

WORKDIR /usr/src/app
COPY ./package.json ./
COPY ./yarn.lock ./
RUN yarn install --production
COPY . .
RUN yarn build

ENV NODE_ENV=production

CMD ["yarn", "start:prod"]
